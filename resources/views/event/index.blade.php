@extends('layouts.root')

@section('title', 'Aktuálne podujatia')
@section('content')

    @forelse($events as $event)
        {{--        {{ $imgUrl = $event->imgUrl == "" ? "URL::asset('img/noImg.png')" : $event->imgUrl }}--}}
        <div class="event">
            <img src="{{$event->imgUrl == '' ? URL::asset('img/noImg.png') : $event->imgUrl}}" class="eventImg">

            <div class="eventName">
                {{ $event->title }}
            </div>
            <div class="eventShortDesc">
                {{ Str::limit($event->description, 100) }}
            </div>
            <div class="eventButton">
                <a href="{{  url('events', $event->id) }}">Viac info</a>
            </div>

            <div class="eventEditDelete">

                {!! Form::open([
                'method' => 'GET',
                'route' => ['events.edit', $event->id]
            ]) !!}
                {!! Form::submit('Upraviť') !!}
                {!! Form::close() !!}

                {!! Form::open([
                'method' => 'DELETE',
                'route' => ['events.destroy', $event->id]
            ]) !!}
                {!! Form::submit('Zmazať') !!}
                {!! Form::close() !!}
            </div>
        </div>
    @empty
        <p>Nič na zobrazenie</p>
    @endforelse

@endsection
